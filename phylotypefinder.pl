#! /usr/bin/perl

# --------------------------------------------------------------------
# %% Setting up %%

use strict;
use warnings;
use Bio::SeqIO;
use Bio::SearchIO;
use Try::Tiny::Retry;
use Getopt::Long;
use File::Basename;
use File::Spec;
use Cwd;
use File::Temp;

use constant PROGRAM_NAME            => 'phylotypefinder.pl';
use constant PROGRAM_NAME_LONG       => 'Predicts E. coli phylotype for draft or complete genome';
use constant VERSION                 => '1.0';

#Global variables
my $prg_blastall =	"blastall";
my $prg_formatdb =	"formatdb";
my ($Help, $DB, $InFile, $dir);
my $IFormat = "fasta";
my %ARGV    = ('-p' => 'blastn', '-a' => '5' , '-F' => 'F', '-e' => '0.0000000001' );
my $phylotype;
my $zero_ones;
my $db_file = "phylotyping_genes.fsa";

#Getting global variables from the command line
&commandline_parsing();

if (defined $Help || not defined $DB || not defined $InFile) {
   print_help();
   exit;
}

if (not defined $dir) {
  $dir = ".";
}


# Making tmp directory for BLAST output
my $tmp_dir = "$dir/tmp";
mkdir("$dir/tmp");

#Copying database fasta file to tmp folder:
my $cp_cmd = "cp " . $DB . "\/" . $db_file . " "  . $tmp_dir . "\/phylotyping_genes.fsa" ;
system ($cp_cmd);

#--------------------------------------------------------------------------------

# Main Program #

#Expected lengths of genes
my($length_tsp, $length_chuA, $length_chuA_476, $length_yjaA, $length_arpA_301, $length_arpA_400, $length_trpa, $length_cladeI) = (152, 288, 476, 211, 301, 400, 220, 316);

#Here we run blast
my @Blast_lines;
retry{
   
   @Blast_lines = get_blast_run($tmp_dir, $InFile , $db_file, %ARGV);
   
}
catch{ die $_ };


my($has_tsp, $has_chuA, $has_chuA_476, $has_yjaA, $has_arpA_301, $has_arpA_400, $has_trpa, $has_cladeI) = (0,0,0,0,0,0,0,0);

my $tag_arpA_400 = "";
my $tag_chuA = "";
my $tag_yjaA = "";
my $tag_tsp = "";
my $tag_trpA = "";
my $tag_arpA_310 = "";

my $align_length = 0;
for my $blast_line (@Blast_lines) {  chomp $blast_line;
 	my @blast_elem = split ("\t",$blast_line);
   	my $query = $blast_elem[0];
   	my $query_length = $blast_elem[1];
   	my $hsp_length = $blast_elem[2];
   	my $gaps = $blast_elem[3];
   	my $ident = $blast_elem[4];
   	my $e = $blast_elem[5];
   	my $bits = $blast_elem[6];
   	my $q_string = $blast_elem[7];
   	my $hit_string = $blast_elem[8];
   	my $homo_string = $blast_elem[9];
   	my @seq_inds = $blast_elem[10];
   	my $hit_strand = $blast_elem[11];
   	my $hit_start = $blast_elem[12];
   	my $hit_end = $blast_elem[13];
   	my $contig_name = $blast_elem[14];
   	my $query_strand = $blast_elem[15];
   	my $query_start = $blast_elem[16];
   	my $hit_length = $blast_elem[17];

    #print "$query, $query_length, $hsp_length, $ident, $e, $bits, $q_string, $hit_string, $homo_string, $hit_strand, $hit_start, $hit_end, $contig_name, $query_strand, $query_start\n";
    
    if($query =~ /chuA_476/){
		if($hsp_length >= ($length_chuA_476-10) && $hsp_length <= ($length_chuA_476+10)){
			$has_chuA_476 = 1;
		}
	}
	elsif($query =~ /chuA/){
		if($hsp_length >= ($length_chuA-10) && $hsp_length <= ($length_chuA+10)){
		#PCR for chuA fails to produce product, not because of lack of gene, but because of point mutations in the primer binding area. 
			my $primer_binding_site_1 = substr($hit_string, 0, 20);
			my $primer_binding_site_2 = substr($hit_string, -20);
			
			
			#Checking for non-allowe primer binding sites
			my $non_allowed_forward_primer_binding_site1 = "atggtacgggacgcacaaat"; #Seen in CladeIII isolate
			my $non_allowed_revers_primer_binding_site1 = "tgtctttggcactggcggca"; #Seen in CladeIII isolate

			my $non_allowed_forward_primer_binding_site2 = "atggtacgggacgcacgaat"; #Seen in CladeIV isolate
			my $non_allowed_revers_primer_binding_site2 = "tgtctttggcactggcggca"; #Seen in CladeIV isolate

			my $non_allowed_reverse_primer_binding_site3 = "tgtctttggtactggtggca"; #Seen in Clade I isolate on its own
			                                                 
			if (($primer_binding_site_1 eq $non_allowed_forward_primer_binding_site1) && ($primer_binding_site_2 eq $non_allowed_revers_primer_binding_site1)){
				$has_chuA = 0;
				$tag_chuA = "PBS problem";
			}
			elsif (($primer_binding_site_1 eq $non_allowed_forward_primer_binding_site2) && ($primer_binding_site_2 eq $non_allowed_revers_primer_binding_site2)){
				$has_chuA = 0;
				$tag_chuA = "PBS problem";
			}
			elsif ($primer_binding_site_2 eq $non_allowed_reverse_primer_binding_site3){
				$has_chuA = 0;
				$tag_chuA = "PBS problem";
			}
			else {
				$has_chuA = 1;
			}	

		} 
	}
	elsif($query =~ /yjaA/){
		if($hsp_length >= ($length_yjaA-10) && $hsp_length <= ($length_yjaA+10)){
			$has_yjaA = 1;}
	}
	elsif($query =~ /TspE4\.C2/){
		if($hsp_length >= ($length_tsp-10) && $hsp_length <= ($length_tsp+10)){
			#PCR for TspE4.C2 fails to produce product, not because of lack of gene, but because of point mutations in the primer binding area. 
			my $primer_binding_site_1 = substr($hit_string, 0, 20);
			my $primer_binding_site_2 = substr($hit_string, -20);
			
			#Checking for non-allowed primer binding sites. These are the primer-binding sites that have been observed in known Clade I phylotypes. 

			my $non_allowed_forward_primer_binding_site1 = "cactattcgtaaggtcatcg"; #Seen in Clade I isolate. The other primer binding site was perfect
			
			
			if ($primer_binding_site_1 eq $non_allowed_forward_primer_binding_site1){
				$has_tsp = 0;
				$tag_tsp = "PBS problem";
			}
			else {
				$has_tsp = 1;
			}
		}

	}
	elsif($query =~ /cladeI/){
		if($hsp_length >= ($length_cladeI-50) && $hsp_length <= ($length_cladeI+10)){
			$has_cladeI = 1;}
	}
	elsif($query =~ /ArpA_301/){
		if($hsp_length >= ($length_arpA_301-10) && $hsp_length <= ($length_arpA_301+10)){
			#PCR for arpA_301 fails to produce product, not because of lack of gene, but because of two point mutations, each the last one in the PCR primers. If these point mutations are present, it counts as if arpA_301 is not there. 
			my $primer_binding_site1 = substr($hit_string, 0, 24);
			my $primer_binding_site2 = substr($hit_string, -24);
			
			#Checking for non-allowed primer binding sites. These are the primer-binding sites that have been observed in known non-E phylotypes. 

			my $non_allowed_forward_primer_binding_site1 = "agcatattttgacaagatggaatc"; #Seen in D isolate in combination with $non_allowed_reverse_primer_binding_site1
			my $non_allowed_forward_primer_binding_site2 = "agcatattttgacaagatggnnnn"; #Seen in D isolate in combination with $non_allowed_reverse_primer_binding_site1
			my $non_allowed_forward_primer_binding_site3 = "tgaaagcatattttgacaagatgg"; #Seen in D isolate in combination with $non_allowed_reverse_primer_binding_site1
			

			my $non_allowed_reverse_primer_binding_site1 = "gaaaagaaaaagaatttccaagaa"; #Seen in D isolate in combination with $non_allowed_forward_primer_binding_site1                                 
			my $non_allowed_reverse_primer_binding_site2 = "gaaaagaaaaagaatttccaagag"; #Seen on its own in Clade I isolate 
			my $non_allowed_reverse_primer_binding_site1a = "gaaaagaaaaagaattcccaagaa"; #Seen in D isolate in combination with $non_allowed_forward_primer_binding_site1 
			
			if (($primer_binding_site1 eq $non_allowed_reverse_primer_binding_site1) && (($primer_binding_site2 eq $non_allowed_forward_primer_binding_site1) or ($primer_binding_site2 eq $non_allowed_forward_primer_binding_site2) or ($primer_binding_site2 eq $non_allowed_forward_primer_binding_site3))) {
				$has_arpA_301 = 0;
				$tag_arpA_310 = "PBS problem";
			}
			elsif ($primer_binding_site1 eq $non_allowed_reverse_primer_binding_site2){
				$has_arpA_301 = 0;
				$tag_arpA_310 = "PBS problem";
			}
			elsif (($primer_binding_site1 eq $non_allowed_reverse_primer_binding_site1a) && ($primer_binding_site2 eq $non_allowed_forward_primer_binding_site1)) {
				$has_arpA_301 = 0;
				$tag_arpA_310 = "PBS problem";
			}
			else {
				$has_arpA_301 = 1;
			}

		}	
	}
	elsif($query =~ /TrpA_220/){
		if($hsp_length >= ($length_trpa-10) && $hsp_length <= ($length_trpa+10)){
			#PCR for TrpA fails to produce product, not because of lack of gene, but because of two point mutations, each the last one in the PCR primers. If these point mutations are present, it counts as if TrpA_220 is not there. 
			my $start_hit_string = substr($hit_string, 0, 20);
			my $significant_base_primer1 = substr($start_hit_string,-1);
			my $significant_base_primer2 = substr($hit_string, -18, 1);
			if (($significant_base_primer1 eq "g") || ($significant_base_primer1 eq "G")){
				if (($significant_base_primer2 eq "g") || ($significant_base_primer2 eq "G")){ 
					$has_trpa = 1;
				}
			}
			
		}
	}
	elsif($query =~ /ArpA_400/){
		if($hsp_length >= ($length_arpA_400-10) && $hsp_length <= ($length_arpA_400+10)){
			#PCR for ARpA_400 fails to produce product, not because of lack of gene, but because of point mutations in the primer binding area. 
			my $primer_binding_site_1 = substr($hit_string, 0, 20);
			my $primer_binding_site_2 = substr($hit_string, -20);

			#Checking for non-allowed primer binding sites.
			my $non_allowed_forward_primer_binding_site1 = "aacgccattcgccagctcgc"; #Seen in CladeIII isolate in combination w $non_allowed_revers_primer_binding_site1
			my $non_allowed_forward_primer_binding_site2 = "aacgccattcgccagcttgc"; #Seen in CladeIII isolate in combination w $non_allowed_revers_primer_binding_site1
			my $non_allowed_forward_primer_binding_site3 = "aacgccattcgccagcttgc"; #Seen in CladeI isolate in combi w $non_allowed_revers_primer_binding_site2
			

			my $non_allowed_revers_primer_binding_site1 = "gcagcggtttagcgtgcggt"; #Seen in CladeIII isolate in combination w $non_allowed_forward_primer_binding_site1
            my $non_allowed_revers_primer_binding_site2 = "tagcgtacgatatggggaga";                                               
			my $non_allowed_revers_primer_binding_site3 = "tagcgtacgatatggggaga";                                               
		
			
			if (($primer_binding_site_1 eq $non_allowed_forward_primer_binding_site1) && ($primer_binding_site_2 eq $non_allowed_revers_primer_binding_site1)){
				$has_arpA_400 = 0;
				$tag_arpA_400 = "PBS problem";
			}
			elsif (($primer_binding_site_1 eq $non_allowed_forward_primer_binding_site2) && ($primer_binding_site_2 eq $non_allowed_revers_primer_binding_site1)){
				$has_arpA_400 = 0;
				$tag_arpA_400 = "PBS problem";
			}
			elsif (($primer_binding_site_1 eq $non_allowed_forward_primer_binding_site3) && ($primer_binding_site_2 eq $non_allowed_revers_primer_binding_site2)){
				$has_arpA_400 = 0;
				$tag_arpA_400 = "PBS problem";
			}
			elsif ($primer_binding_site_2 eq $non_allowed_revers_primer_binding_site3){
				$has_arpA_400 = 0;
				$tag_arpA_400 = "PBS problem";
			}
			else {
				$has_arpA_400 = 1;
			}
		}
	}

		
	
}	
	#print "has_arpA_400: " . $has_arpA_400 . " $tag_arpA_400" . "\n";
	#print "has_chuA: " . $has_chuA . " " . $tag_chuA . "\n";
	#print "has_yjaA: " . $has_yjaA . " " .  $tag_yjaA . "\n";
	#print "has_tsp: " . $has_tsp . " " . $tag_tsp . "\n";
	#print "has_arpA_301: " . $has_arpA_301 . " " . $tag_arpA_310 . "\n";
	#print "has_trpa: " . $has_trpa . " " . $tag_trpA . "\n";
	#print "has_cladeI: " . $has_cladeI . "\n";
	#print "has_chuA_476: " . $has_chuA_476  . "\n";

	if($has_tsp){
		#It is either B1, B2, D, or E
		if($has_chuA){
		#It is either B2, D, or E	
			if($has_yjaA){
			#It is B2	
				if ($has_arpA_400 == 0){
					$phylotype = "B2";
					#$zero_ones = ("PCR product\t$has_arpA_400\t$has_chuA\t$has_yjaA\t$has_tsp\t$has_trpa\t$has_arpA_301\t$has_cladeI\t$has_chuA_476\n");
					$zero_ones = ("PCR product\t$has_arpA_400\t$has_chuA\t$has_yjaA\t$has_tsp\tNA\tNA\tNA\tNA\n");
				}
				else {
					$phylotype = "Unknown phylo-group";
					$zero_ones = ("PCR product\t$has_arpA_400\t$has_chuA\t$has_yjaA\t$has_tsp\t$has_trpa\t$has_arpA_301\t$has_cladeI\t$has_chuA_476\n");
					#$zero_ones = ("PCR product\t$has_arpA_400\t$has_chuA\t$has_yjaA\t$has_tsp\tNA\tNA\tNA\tNA\n");
				}
			}
			else {
			#It is either B2, D, or E (yes, some B2 do not have yjaA)
				if($has_arpA_400){
				#It is either D or E	
					if($has_arpA_301){
					#It is E	
						$phylotype = "E";
					    #$zero_ones = ("PCR product\t$has_arpA_400\t$has_chuA\t$has_yjaA\t$has_tsp\t$has_trpa\t$has_arpA_301\t$has_cladeI\t$has_chuA_476\n");
					    $zero_ones = ("PCR product\t$has_arpA_400\t$has_chuA\t$has_yjaA\t$has_tsp\tNA\t$has_arpA_301\tNA\tNA\n");
					}
					else {
					#It is D
						$phylotype = "D";
					    #$zero_ones = ("PCR product\t$has_arpA_400\t$has_chuA\t$has_yjaA\t$has_tsp\t$has_trpa\t$has_arpA_301\t$has_cladeI\t$has_chuA_476\n");
					    $zero_ones = ("PCR product\t$has_arpA_400\t$has_chuA\t$has_yjaA\t$has_tsp\tNA\t$has_arpA_301\tNA\tNA\n");
					}
				}
				else {
				#It is B2
					$phylotype = "B2";
					#$zero_ones = ("PCR product\t$has_arpA_400\t$has_chuA\t$has_yjaA\t$has_tsp\t$has_trpa\t$has_arpA_301\t$has_cladeI\t$has_chuA_476\n");
					$zero_ones = ("PCR product\t$has_arpA_400\t$has_chuA\t$has_yjaA\t$has_tsp\tNA\tNA\tNA\tNA\n");
				}
			}	
		}
		else {
			if (($has_yjaA == 0) && ($has_arpA_400)){
			$phylotype = "B1";
			#$zero_ones = ("PCR product\t$has_arpA_400\t$has_chuA\t$has_yjaA\t$has_tsp\t$has_trpa\t$has_arpA_301\t$has_cladeI\t$has_chuA_476\n");
			$zero_ones = ("PCR product\t$has_arpA_400\t$has_chuA\t$has_yjaA\t$has_tsp\tNA\tNA\tNA\tNA\n");
			}
			else {
				$phylotype = "Unknown phylo-group";
				$zero_ones = ("PCR product\t$has_arpA_400\t$has_chuA\t$has_yjaA\t$has_tsp\t$has_trpa\t$has_arpA_301\t$has_cladeI\t$has_chuA_476\n");
				#$zero_ones = ("PCR product\t$has_arpA_400\t$has_chuA\t$has_yjaA\t$has_tsp\tNA\tNA\tNA\tNA\n");
			}
		}
		
	}
	else {
	#It is either A, F, B2, C, D, E, Clade I, Clade I/II, or Clade III/IV/V
		if($has_chuA){
			#It is F, B2, D, E, or Clade I	
			if($has_yjaA){
			#It is B2, E, or Clade I
				if ($has_arpA_400){
				#It is E or Clade I
					if ($has_arpA_301){
					#It is E
						$phylotype = "E";
					    #$zero_ones = ("PCR product\t$has_arpA_400\t$has_chuA\t$has_yjaA\t$has_tsp\t$has_trpa\t$has_arpA_301\t$has_cladeI\t$has_chuA_476\n");
					    $zero_ones = ("PCR product\t$has_arpA_400\t$has_chuA\t$has_yjaA\t$has_tsp\tNA\t$has_arpA_301\t$has_cladeI\tNA\n");
					}
					else {
					#It is Clade I
						$phylotype = "Clade I";
						#$zero_ones = ("PCR product\t$has_arpA_400\t$has_chuA\t$has_yjaA\t$has_tsp\t$has_trpa\t$has_arpA_301\t$has_cladeI\t$has_chuA_476\n");
						$zero_ones = ("PCR product\t$has_arpA_400\t$has_chuA\t$has_yjaA\t$has_tsp\tNA\t$has_arpA_301\t$has_cladeI\tNA\n");
					}
				}
				else {
				#It is B2
					$phylotype = "B2";
					#$zero_ones = ("PCR product\t$has_arpA_400\t$has_chuA\t$has_yjaA\t$has_tsp\t$has_trpa\t$has_arpA_301\t$has_cladeI\t$has_chuA_476\n");
					$zero_ones = ("PCR product\t$has_arpA_400\t$has_chuA\t$has_yjaA\t$has_tsp\tNA\tNA\tNA\tNA\n");
				}

			}	
			else {
			#It is F, D, or E
			if ($has_arpA_400){
				#It is D or E
					if ($has_arpA_301){
					#It is E
						$phylotype = "E";
						#$zero_ones = ("PCR product\t$has_arpA_400\t$has_chuA\t$has_yjaA\t$has_tsp\t$has_trpa\t$has_arpA_301\t$has_cladeI\t$has_chuA_476\n");
						$zero_ones = ("PCR product\t$has_arpA_400\t$has_chuA\t$has_yjaA\t$has_tsp\tNA\t$has_arpA_301\tNA\tNA\n");
					}
					else {
					#It is D
						$phylotype = "D";
						#$zero_ones = ("PCR product\t$has_arpA_400\t$has_chuA\t$has_yjaA\t$has_tsp\t$has_trpa\t$has_arpA_301\t$has_cladeI\t$has_chuA_476\n");
						$zero_ones = ("PCR product\t$has_arpA_400\t$has_chuA\t$has_yjaA\t$has_tsp\tNA\t$has_arpA_301\tNA\tNA\n");
					}
				}
				else {
				#It is F
					$phylotype = "F";
					#$zero_ones = ("PCR product\t$has_arpA_400\t$has_chuA\t$has_yjaA\t$has_tsp\t$has_trpa\t$has_arpA_301\t$has_cladeI\t$has_chuA_476\n");
					$zero_ones = ("PCR product\t$has_arpA_400\t$has_chuA\t$has_yjaA\t$has_tsp\tNA\tNA\tNA\tNA\n");
				}
			}
		}
		elsif ($has_chuA_476){
			if (($has_yjaA==0)&&($has_arpA_400==0)){
				#It is clade III/IV/V
				$phylotype = "Clade III/IV/V";
				$zero_ones = ("PCR product\t$has_arpA_400\t$has_chuA\t$has_yjaA\t$has_tsp\tNA\tNA\t$has_cladeI\t$has_chuA_476\n");
			}
			elsif (($has_yjaA==1)&&($has_arpA_400==0)){ 
				$phylotype = "Clade I/II";
				$zero_ones = ("PCR product\t$has_arpA_400\t$has_chuA\t$has_yjaA\t$has_tsp\tNA\tNA\t$has_cladeI\tNA\n");
			 }
			else { 
				$phylotype = "Unknown phylo-group";
				$zero_ones = ("PCR product\t$has_arpA_400\t$has_chuA\t$has_yjaA\t$has_tsp\t$has_trpa\t$has_arpA_301\t$has_cladeI\t$has_chuA_476\n");
			 }
		}
		else {
			#It is A, C, or Clade I/II
			if($has_yjaA){
			#It is A, C or Clade I/II (yes, some A has yjaA)	
				if($has_arpA_400){
				#It is A or C	
					if($has_trpa){
					#It is C	
						$phylotype = "C";
						#$zero_ones = ("PCR product\t$has_arpA_400\t$has_chuA\t$has_yjaA\t$has_tsp\t$has_trpa\t$has_arpA_301\t$has_cladeI\t$has_chuA_476\n");
						$zero_ones = ("PCR product\t$has_arpA_400\t$has_chuA\t$has_yjaA\t$has_tsp\t$has_trpa\tNA\tNA\tNA\n");
					}
					else {
					#It is A
						$phylotype = "A";
						#$zero_ones = ("PCR product\t$has_arpA_400\t$has_chuA\t$has_yjaA\t$has_tsp\t$has_trpa\t$has_arpA_301\t$has_cladeI\t$has_chuA_476\n");
						$zero_ones = ("PCR product\t$has_arpA_400\t$has_chuA\t$has_yjaA\t$has_tsp\t$has_trpa\tNA\tNA\tNA\n");
					}	
				}
				else {
				#It is CladeI/II
					$phylotype = "CladeI/II";
					#$zero_ones = ("PCR product\t$has_arpA_400\t$has_chuA\t$has_yjaA\t$has_tsp\t$has_trpa\t$has_arpA_301\t$has_cladeI\t$has_chuA_476\n");
					$zero_ones = ("PCR product\t$has_arpA_400\t$has_chuA\t$has_yjaA\t$has_tsp\tNA\tNA\t$has_cladeI\tNA\n");
				}
			}
			else {
			#It is A
				if ($has_arpA_400){
					$phylotype = "A";
					#$zero_ones = ("PCR product\t$has_arpA_400\t$has_chuA\t$has_yjaA\t$has_tsp\t$has_trpa\t$has_arpA_301\t$has_cladeI\t$has_chuA_476\n");
					$zero_ones = ("PCR product\t$has_arpA_400\t$has_chuA\t$has_yjaA\t$has_tsp\tNA\tNA\tNA\tNA\n");
				}
				else {
					$phylotype = "Unknown phylo-group";
					$zero_ones = ("PCR product\t$has_arpA_400\t$has_chuA\t$has_yjaA\t$has_tsp\t$has_trpa\t$has_arpA_301\t$has_cladeI\t$has_chuA_476\n");
				}
			}

		}
	}

#Removing temporary files generated by blast

system("rm -r error.log");
system("rm -r formatdb.log");
system("rm -r  $tmp_dir/");



# -------------------------
# Generating output files #

#Outputfile
open(TABR, '>'."$dir/results_tab.txt") or die ("Couldn't write to file results_tab.txt\n");
my $indledende_tabr = "Phylotype:" . "\t" . $phylotype . "\n";
print TABR $indledende_tabr;
my $header = "Locus\tarpA \($length_arpA_400 bp\)\tchuA \($length_chuA bp\)\tyjaA \($length_yjaA bp\)\tTspE4\.C2 \($length_tsp bp\)\ttrpA \($length_trpa bp\)\tarpA \($length_arpA_301 bp\)\tClade I \($length_cladeI\)\tchuA_476\n";
print TABR $header;
print TABR $zero_ones;


close(TABR);

############### LAND OF THE SUBROUTINES ##############

sub commandline_parsing {
    while (scalar @ARGV) {
        if ($ARGV[0] =~ m/^-d$/) {
            $DB = $ARGV[1];
            shift @ARGV;
            shift @ARGV;
        }
        elsif ($ARGV[0] =~ m/^-i$/) {
            $InFile = $ARGV[1];
            shift @ARGV;
            shift @ARGV;
        }
        elsif ($ARGV[0] =~ m/^-o$/) {
            $dir = $ARGV[1];
            mkdir $dir;
            shift @ARGV;
            shift @ARGV;
        }
        elsif ($ARGV[0] =~ m/^-h$/) {
            $Help = 1;
            shift @ARGV;
        }
        else {
         &print_help();
         exit
        }
    }
}

sub get_blast_run {
   
   	my ($tmp_dir, $infile, $dbfile, %args) = @_;
   	my @split_infile = split("\/",$infile);
   	my $last_elem = pop @split_infile;
   	my $file = "blast_$last_elem";
   	
	#Copying infile (the draft genome) to tmp folder:
	my $cp_cmd1 = "cp " . $infile . " "  . $tmp_dir . "\/$file" ;
	system ($cp_cmd1); 
	
   die "Error! Could not build blast database" if (system("$prg_formatdb  -p F -i $tmp_dir/$file"));
   
   
   my $cmd = join(" ", %args);
   my $file2 = "$tmp_dir/$file.blast_output";
   my $Kommando = "$prg_blastall -d $tmp_dir/$file -i $tmp_dir/$dbfile -o $file2 $cmd";
   #print "Her er blast kommandoen: " . $Kommando . "\n";  
   system($Kommando);

   my $report = new Bio::SearchIO( -file   => $file2,
                                   -format => "blast"
                                 );
   # Go through BLAST reports one by one
   my @blast;
   while(my $result = $report->next_result) {
      # Go through each matching sequence
      while(my $hit = $result->next_hit)    {
         # Go through each each HSP for this sequence
         while (my$hsp = $hit->next_hsp)  {
            push(@blast, $result->query_accession ."\t".
                        $result->query_length ."\t".
                        $hsp->hsp_length ."\t".
                        $hsp->gaps ."\t".
                        $hsp->percent_identity ."\t".
                        $hsp->evalue ."\t".
                        $hsp->bits ."\t".
                        $hsp->query_string ."\t".
                        $hsp->hit_string ."\t".
                        $hsp->homology_string ."\t".
                        $hsp->seq_inds ."\t".
                        $hsp->strand('hit') ."\t".
                        $hsp->start('hit') ."\t".
                        $hsp->end('hit') ."\t".
                        $hit->name ."\t".
                        $hsp->strand('query') ."\t".
                        $hsp->start('query') ."\t".
                        $hit->length ."\n");
         }
      }

   }
   return @blast;
}



sub print_help {
  my $ProgName     = PROGRAM_NAME;
  my $ProgNameLong = PROGRAM_NAME_LONG;
  my $Version      = VERSION;
  my $CMD = join(" ", %ARGV);
  print <<EOH;


NAME
  $ProgName - $ProgNameLong

SYNOPSIS
  $ProgName -d [database] -i [File] [Options]

DESCRIPTION
  TBA

OPTIONS
  
  -h HELP
                    Prints a message with options and information to the screen
    -d DATABASE
                    The path to where you have located the database folder
    -b BLAST
                    The path to the location of blast-2.2.26 if it is not added
                    to the user's path (see the install guide in 'README.md')
    -i INFILE
                    Your input file which needs to be preassembled partial
                    or complete genomes in fasta format
    -o OUTFOLDER
                    The folder you want to have your output files places.
                    If not specified the program will create a folder named
                    'Output' in which the result files will be stored.
  

Example of use with the 'database' folder located in the current directory and Blast added to the user's path
    
    perl phylotypefinder.pl -i test.fsa -o OUTFOLDER 

Example of use with the 'database' and 'blast-2.2.26' folders loacted in other directories

    perl phylotypefinder.pl -d path/to/database -b path/to/blast-2.2.26 -i test.fsa -o OUTFOLDER 
 
  
VERSION
    Current: $Version

AUTHORS
    Mette Voldby Larsen

EOH
}